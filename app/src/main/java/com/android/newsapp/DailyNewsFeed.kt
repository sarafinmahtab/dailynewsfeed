package com.android.newsapp

import android.app.Application
import com.android.login.di.LoginComponent
import com.android.login.di.LoginComponentProvider
import com.android.news.di.NewsComponent
import com.android.news.di.NewsComponentProvider
import com.android.newsapp.di.AppComponent
import com.android.newsapp.di.DaggerAppComponent
import com.android.weather.di.WeatherComponent
import com.android.weather.di.WeatherComponentProvider

/**
 * @author Shamsul Arafin Mahtab
 * @since 25/02/2021
 */

class DailyNewsFeed : Application(),
    NewsComponentProvider, WeatherComponentProvider, LoginComponentProvider {

    val appComponent: AppComponent by lazy {
        initializeComponent()
    }

    private fun initializeComponent(): AppComponent {
        return DaggerAppComponent.factory().create(applicationContext)
    }

    override fun provideNewsComponent(): NewsComponent {
        return appComponent.newsComponent().create()
    }

    override fun provideWeatherComponent(): WeatherComponent {
        return appComponent.weatherComponent().create()
    }

    override fun provideLoginComponent(): LoginComponent {
        return appComponent.loginComponent().create()
    }
}
