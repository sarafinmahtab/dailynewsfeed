package com.android.newsapp.di

import com.android.core.usecase.IntentLauncher
import com.android.core.usecase.UserContext
import com.android.newsapp.usecase.IntentLauncherImpl
import com.android.newsapp.usecase.UserContextImpl
import dagger.Binds
import dagger.Module

@Module
abstract class AppModule {

    @Binds
    abstract fun bindsUserContext(userContextImpl: UserContextImpl): UserContext

    @Binds
    abstract fun bindsIntentLauncher(intentLauncherImpl: IntentLauncherImpl): IntentLauncher
}
