package com.android.newsapp.di

import dagger.Module

/**
 * @author Shamsul Arafin Mahtab
 * @since 3/16/2021
 */

@Module
abstract class MainDiModule
