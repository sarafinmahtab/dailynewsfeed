package com.android.newsapp.di

import dagger.Module

/**
 * @author Shamsul Arafin Mahtab
 * @since 16/03/2021
 */

@Module(
    subcomponents = [MainComponent::class]
)
class SubComponentModule
