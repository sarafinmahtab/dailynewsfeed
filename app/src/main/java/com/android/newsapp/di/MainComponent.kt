package com.android.newsapp.di

import com.android.newsapp.ui.main.MainActivity
import dagger.Subcomponent

/**
 * @author Shamsul Arafin Mahtab
 * @since 16/03/2021
 */

@MainScope
@Subcomponent(modules = [MainDiModule::class])
interface MainComponent {

    fun inject(activity: MainActivity)

    @Subcomponent.Factory
    interface Factory {
        fun create(): MainComponent
    }
}
