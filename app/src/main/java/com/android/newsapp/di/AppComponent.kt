package com.android.newsapp.di

import android.content.Context
import com.android.core.di.AppScope
import com.android.core.di.CoreModule
import com.android.core.di.NetworkModule
import com.android.login.di.LoginComponent
import com.android.news.di.NewsComponent
import com.android.weather.di.WeatherComponent
import dagger.BindsInstance
import dagger.Component

/**
 * @author Shamsul Arafin Mahtab
 * @since 02/03/2021
 */

@AppScope
@Component(
    modules = [
        CoreModule::class,
        NetworkModule::class,
        AppModule::class,
        SubComponentModule::class
    ]
)
interface AppComponent {

    @Component.Factory
    interface Factory {
        /**
         * @param context With @BindsInstance, the Context passed in will be available in the graph
         */
        fun create(@BindsInstance context: Context): AppComponent
    }

    fun mainComponent(): MainComponent.Factory

    fun newsComponent(): NewsComponent.Factory

    fun weatherComponent(): WeatherComponent.Factory

    fun loginComponent(): LoginComponent.Factory
}
