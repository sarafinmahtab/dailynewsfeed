package com.android.newsapp.di

import javax.inject.Scope

/**
 * @author Shamsul Arafin Mahtab
 * @since 16/03/2021
 */

@Scope
@Retention(value = AnnotationRetention.RUNTIME)
annotation class MainScope
