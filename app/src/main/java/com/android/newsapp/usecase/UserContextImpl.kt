package com.android.newsapp.usecase

import com.android.core.di.AppScope
import com.android.core.model.LoginUser
import com.android.core.usecase.UserContext
import javax.inject.Inject


/**
 * @author Shamsul Arafin Mahtab
 * @since 16/03/2021
 */

@AppScope
class UserContextImpl @Inject constructor() : UserContext {

    private lateinit var mLoginUser: LoginUser

    override fun saveLoginUser(loginUser: LoginUser) {
        this.mLoginUser = loginUser
    }

    override fun getLoginUser(): LoginUser {
        return mLoginUser
    }

    override fun isUserLoggedIn(): Boolean {
        return ::mLoginUser.isInitialized
    }
}
