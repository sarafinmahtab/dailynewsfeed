package com.android.newsapp.usecase

import android.app.Activity
import android.content.Intent
import com.android.core.usecase.IntentLauncher
import com.android.core.di.AppScope
import com.android.login.ui.login.LoginActivity
import com.android.newsapp.ui.main.MainActivity
import javax.inject.Inject

/**
 * @author Shamsul Arafin Mahtab
 * @since 16/03/2021
 */

@AppScope
class IntentLauncherImpl @Inject constructor() : IntentLauncher {

    override fun getNewsFeedIntent(activity: Activity): Intent {
        return Intent(activity, MainActivity::class.java)
    }

    override fun getLoginIntent(activity: Activity): Intent {
        return Intent(activity, LoginActivity::class.java)
    }
}
