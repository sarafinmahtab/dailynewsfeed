package com.android.newsapp.ui.main

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.android.news.ui.newsstream.NewsStreamFragment
import com.android.newsapp.DailyNewsFeed
import com.android.newsapp.R
import com.android.newsapp.databinding.ActivityMainBinding
import com.android.weather.ui.weather.WeatherFragment

/**
 * @author Shamsul Arafin Mahtab
 * @since 25/02/2021
 */

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        val mainComponent =
            (applicationContext as DailyNewsFeed).appComponent.mainComponent().create()
        mainComponent.inject(this)

        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportFragmentManager.beginTransaction()
            .replace(R.id.weatherFrameLayout, WeatherFragment.newInstance(), WeatherFragment.TAG)
            .commit()

        supportFragmentManager.beginTransaction()
            .replace(R.id.newsFrameLayout, NewsStreamFragment.newInstance(), NewsStreamFragment.TAG)
            .commit()
    }

    companion object {

        @JvmStatic
        fun start(activity: Activity) {
            val intent = Intent(activity, MainActivity::class.java)
            activity.startActivity(intent)
            activity.finish()
        }
    }
}
