package com.android.weather.data.network

import com.android.weather.model.WeatherResponse
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * @author Shamsul Arafin Mahtab
 * @since 08/03/2021
 */

interface WeatherApiService {

    @GET("weather")
    suspend fun getCurrentWeatherForCity(
        @Query("q") city: String,
        @Query("appid") appId: String
    ): WeatherResponse
}
