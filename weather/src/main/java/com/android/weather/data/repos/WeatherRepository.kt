package com.android.weather.data.repos

import com.android.core.helpers.Results
import com.android.weather.model.WeatherResponse

/**
 * @author Shamsul Arafin Mahtab
 * @since 08/03/2021
 */

interface WeatherRepository {

    suspend fun getCurrentWeatherForCity(
        city: String,
        appId: String
    ): Results<WeatherResponse>
}
