package com.android.weather.data

/**
 * @author Shamsul Arafin Mahtab
 * @since 08/03/2021
 */

object WeatherServerConstant {
    const val BASE_URL = "https://api.openweathermap.org/data/2.5/"
}
