package com.android.weather.data.repos

import com.android.core.helpers.ResolveApiResponse
import com.android.core.helpers.Results
import com.android.weather.data.network.WeatherApiService
import com.android.weather.di.WeatherScope
import com.android.weather.model.WeatherResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

/**
 * @author Shamsul Arafin Mahtab
 * @since 08/03/2021
 */

@WeatherScope
class WeatherRepositoryImpl @Inject constructor(
    private val weatherApiService: WeatherApiService,
    private val resolveApiResponse: ResolveApiResponse
) : WeatherRepository {

    override suspend fun getCurrentWeatherForCity(
        city: String,
        appId: String
    ): Results<WeatherResponse> = withContext(Dispatchers.IO) {

        return@withContext resolveApiResponse.resolve(javaClass.simpleName) {
            weatherApiService.getCurrentWeatherForCity(city, appId)
        }
    }
}
