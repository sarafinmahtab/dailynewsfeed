package com.android.weather.di

/**
 * @author Shamsul Arafin Mahtab
 * @since 08/03/2021
 */

interface WeatherComponentProvider {
    fun provideWeatherComponent(): WeatherComponent
}
