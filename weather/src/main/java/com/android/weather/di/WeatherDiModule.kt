package com.android.weather.di

import androidx.lifecycle.ViewModel
import com.android.core.di.Names
import com.android.core.di.ViewModelKey
import com.android.weather.data.WeatherServerConstant
import com.android.weather.data.network.WeatherApiService
import com.android.weather.data.repos.WeatherRepository
import com.android.weather.data.repos.WeatherRepositoryImpl
import com.android.weather.ui.weather.WeatherViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named

/**
 * @author Shamsul Arafin Mahtab
 * @since 03/03/2021
 */

@Module
abstract class WeatherDiModule {

    companion object {

        @JvmStatic
        @Named(Names.WEATHER_API)
        @WeatherScope
        @Provides
        fun provideWeatherApiRetrofit(
            okHttpClientBuilder: OkHttpClient.Builder,
            gsonConverterFactory: GsonConverterFactory
        ): Retrofit {
            return Retrofit.Builder()
                .baseUrl(WeatherServerConstant.BASE_URL)
                .addConverterFactory(gsonConverterFactory)
                .client(okHttpClientBuilder.build())
                .build()
        }

        @JvmStatic
        @WeatherScope
        @Provides
        fun provideWeatherApiService(
            @Named(Names.WEATHER_API) retrofit: Retrofit
        ): WeatherApiService {
            return retrofit.create(WeatherApiService::class.java)
        }
    }

    @Binds
    abstract fun bindsWeatherRepository(weatherRepositoryImpl: WeatherRepositoryImpl): WeatherRepository

    @IntoMap
    @ViewModelKey(WeatherViewModel::class)
    @Binds
    abstract fun bindsWeatherViewModel(weatherViewModel: WeatherViewModel): ViewModel
}
