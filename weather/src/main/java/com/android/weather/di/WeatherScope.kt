package com.android.weather.di

import javax.inject.Scope

/**
 * @author Shamsul Arafin Mahtab
 * @since 08/03/2021
 */

@Scope
@Retention(value = AnnotationRetention.RUNTIME)
annotation class WeatherScope
