package com.android.weather.di

import com.android.weather.ui.weather.WeatherFragment
import dagger.Subcomponent

/**
 * @author Shamsul Arafin Mahtab
 * @since 08/03/2021
 */

@WeatherScope
@Subcomponent(
    modules = [WeatherDiModule::class]
)
interface WeatherComponent {

    @Subcomponent.Factory
    interface Factory {

        fun create(): WeatherComponent
    }

    fun inject(weatherFragment: WeatherFragment)
}
