package com.android.weather.model

import com.google.gson.annotations.SerializedName

/**
 * @author Shamsul Arafin Mahtab
 * @since 09/03/2021
 */

data class Wind(
    @field:SerializedName("speed")
    val speed: Double,
    @field:SerializedName("deg")
    val deg: Int
)
