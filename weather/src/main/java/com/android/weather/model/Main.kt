package com.android.weather.model

import com.google.gson.annotations.SerializedName

/**
 * @author Shamsul Arafin Mahtab
 * @since 09/03/2021
 */

data class Main(
    @field:SerializedName("temp")
    val temp: Double,
    @field:SerializedName("feels_like")
    val feelsLike: Double,
    @field:SerializedName("temp_min")
    val tempMin: Double,
    @field:SerializedName("temp_max")
    val tempMax: Double,
    @field:SerializedName("pressure")
    val pressure: Int,
    @field:SerializedName("humidity")
    val humidity: Int
)
