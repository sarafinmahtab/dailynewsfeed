package com.android.weather.model

import com.google.gson.annotations.SerializedName

/**
 * @author Shamsul Arafin Mahtab
 * @since 09/03/2021
 */

data class Coord(
    @field:SerializedName("lon")
    val lon: Double,
    @field:SerializedName("lat")
    val lat: Double
)
