package com.android.weather.model

import com.google.gson.annotations.SerializedName

/**
 * @author Shamsul Arafin Mahtab
 * @since 09/03/2021
 */

data class Sys(
    @field:SerializedName("type")
    val type: Int,
    @field:SerializedName("id")
    val id: Int,
    @field:SerializedName("country")
    val country: String,
    @field:SerializedName("sunrise")
    val sunrise: Int,
    @field:SerializedName("sunset")
    val sunset: Int
)
