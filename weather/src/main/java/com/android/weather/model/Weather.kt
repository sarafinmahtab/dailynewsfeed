package com.android.weather.model

import com.google.gson.annotations.SerializedName

/**
 * @author Shamsul Arafin Mahtab
 * @since 09/03/2021
 */

data class Weather(
    @field:SerializedName("id")
    val id: Int,
    @field:SerializedName("main")
    val main: String,
    @field:SerializedName("description")
    val description: String,
    @field:SerializedName("icon")
    val icon: String
)
