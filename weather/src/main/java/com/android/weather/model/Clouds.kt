package com.android.weather.model

import com.google.gson.annotations.SerializedName

/**
 * @author Shamsul Arafin Mahtab
 * @since 09/03/2021
 */

data class Clouds(
    @field:SerializedName("all")
    val all: Int = 0
)
