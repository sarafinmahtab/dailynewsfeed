package com.android.weather.ui.weather

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.android.core.di.ViewModelFactory
import com.android.core.usecase.UserContext
import com.android.weather.R
import com.android.weather.databinding.FragmentWeatherBinding
import com.android.weather.di.WeatherComponentProvider
import javax.inject.Inject

/**
 * @author Shamsul Arafin Mahtab
 * @since 08/03/2021
 */

class WeatherFragment : Fragment() {

    private val currentCity: String = "Dhaka"

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private lateinit var viewModel: WeatherViewModel

    private lateinit var binding: FragmentWeatherBinding

    @Inject
    lateinit var userContext: UserContext

    override fun onAttach(context: Context) {
        val weatherComponent =
            (requireContext().applicationContext as WeatherComponentProvider).provideWeatherComponent()
        weatherComponent.inject(this)

        viewModel = ViewModelProvider(this, viewModelFactory).get(WeatherViewModel::class.java)

        super.onAttach(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentWeatherBinding.inflate(inflater, container, false)

        viewModel.weatherUpdatesLive.observe(this, {
            val celsiusTemp = it.main.temp - 273.15 // Kelvin to Celsius conversion
            binding.weatherUpdateTextView.text = requireContext().getString(
                R.string.weather_formatted_text,
                celsiusTemp,
                it.weather!!.first().main,
                currentCity
            )

            Toast.makeText(
                requireContext(),
                getString(R.string.welcome_user, userContext.getLoginUser().userName),
                Toast.LENGTH_LONG
            ).show()
        })

        viewModel.getCurrentWeatherForCity(currentCity)

        return binding.root
    }

    companion object {
        const val TAG = "WeatherFragment"

        @JvmStatic
        fun newInstance() = WeatherFragment()
    }
}
