package com.android.weather.ui.weather

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.core.helpers.Results
import com.android.weather.BuildConfig
import com.android.weather.data.repos.WeatherRepository
import com.android.weather.di.WeatherScope
import com.android.weather.model.WeatherResponse
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * @author Shamsul Arafin Mahtab
 * @since 08/03/2021
 */

@WeatherScope
class WeatherViewModel @Inject constructor(
    private val weatherRepository: WeatherRepository
) : ViewModel() {

    private val _weatherUpdatesLive = MutableLiveData<WeatherResponse>()
    val weatherUpdatesLive: LiveData<WeatherResponse> = _weatherUpdatesLive

    fun getCurrentWeatherForCity(currentCity: String) {
        viewModelScope.launch {
            when (val result = weatherRepository.getCurrentWeatherForCity(
                currentCity,
                BuildConfig.WEATHER_API_KEY
            )) {
                is Results.Success -> {
                    _weatherUpdatesLive.postValue(result.value)
                }
                is Results.Failure -> {
                    Log.w(TAG, "+getCurrentWeatherForCity -> ${result.throwable}")
                }
            }
        }
    }

    companion object {
        const val TAG = "WeatherViewModel"
    }
}
