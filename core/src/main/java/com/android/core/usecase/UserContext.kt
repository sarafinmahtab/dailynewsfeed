package com.android.core.usecase

import com.android.core.model.LoginUser


/**
 * @author Shamsul Arafin Mahtab
 * @since 16/03/2021
 */

interface UserContext {

    fun saveLoginUser(loginUser: LoginUser)

    fun getLoginUser(): LoginUser

    fun isUserLoggedIn(): Boolean
}
