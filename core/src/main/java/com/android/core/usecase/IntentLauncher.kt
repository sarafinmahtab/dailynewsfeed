package com.android.core.usecase

import android.app.Activity
import android.content.Intent


/**
 * @author Shamsul Arafin Mahtab
 * @since 16/03/2021
 */

interface IntentLauncher {

    fun getNewsFeedIntent(activity: Activity): Intent

    fun getLoginIntent(activity: Activity): Intent
}
