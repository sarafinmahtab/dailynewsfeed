package com.android.core.di

import javax.inject.Scope


@Scope
@Retention(value = AnnotationRetention.RUNTIME)
annotation class AppScope
