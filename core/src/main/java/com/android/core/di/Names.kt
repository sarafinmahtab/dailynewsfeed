package com.android.core.di


object Names {
    const val WEATHER_API = "WeatherApi"
    const val NEWS_API = "NewsApi"
}
