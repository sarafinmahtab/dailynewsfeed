package com.android.core.helpers

import android.util.Log
import com.android.core.di.AppScope
import javax.inject.Inject

/**
 * @author Shamsul Arafin Mahtab
 * @since 04/03/2021
 */

@AppScope
class ResolveApiResponse @Inject constructor() {

    suspend fun <T> resolve(classTag: String, updateCall: suspend () -> T) =
        try {
            Results.Success(updateCall.invoke())
        } catch (throwable: Throwable) {
            Log.w(classTag, throwable)
            throwable.printStackTrace()
            Results.Failure(throwable)
        }
}
