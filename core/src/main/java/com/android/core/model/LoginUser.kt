package com.android.core.model

import java.util.*

/**
 * @author Shamsul Arafin Mahtab
 * @since 15/03/2021
 */

data class LoginUser(
    val userId: String,
    val userName: String,
    val password: String
) {
    constructor(userName: String, password: String) : this(
        UUID.randomUUID().toString(),
        userName,
        password
    )
}
