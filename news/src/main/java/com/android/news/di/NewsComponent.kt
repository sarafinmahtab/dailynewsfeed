package com.android.news.di

import com.android.news.ui.newsstream.NewsStreamFragment
import dagger.Subcomponent

/**
 * @author Shamsul Arafin Mahtab
 * @since 03/03/2021
 */

@NewsScope
@Subcomponent(
    modules = [NewsDiModule::class]
)
interface NewsComponent {

    fun inject(newsStreamFragment: NewsStreamFragment)

    @Subcomponent.Factory
    interface Factory {

        fun create(): NewsComponent
    }
}
