package com.android.news.di

import javax.inject.Scope

/**
 * @author Shamsul Arafin Mahtab
 * @since 03/03/2021
 */

@Scope
@Retention(value = AnnotationRetention.RUNTIME)
annotation class NewsScope
