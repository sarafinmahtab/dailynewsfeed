package com.android.news.di

/**
 * @author Shamsul Arafin Mahtab
 * @since 04/03/2021
 */

interface NewsComponentProvider {
    fun provideNewsComponent(): NewsComponent
}
