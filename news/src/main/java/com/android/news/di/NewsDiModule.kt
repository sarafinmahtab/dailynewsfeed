package com.android.news.di

import androidx.lifecycle.ViewModel
import com.android.core.di.Names
import com.android.core.di.ViewModelKey
import com.android.news.data.NewsServerConstant
import com.android.news.data.network.NewsApiService
import com.android.news.data.repos.NewsRepository
import com.android.news.data.repos.NewsRepositoryImpl
import com.android.news.ui.newsstream.NewsStreamViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named

/**
 * @author Shamsul Arafin Mahtab
 * @since 03/03/2021
 */

@Module
abstract class NewsDiModule {

    companion object {

        @JvmStatic
        @Named(Names.NEWS_API)
        @NewsScope
        @Provides
        fun provideNewsApiRetrofit(
            okHttpClientBuilder: OkHttpClient.Builder,
            gsonConverterFactory: GsonConverterFactory
        ): Retrofit {
            return Retrofit.Builder()
                .baseUrl(NewsServerConstant.BASE_URL)
                .addConverterFactory(gsonConverterFactory)
                .client(okHttpClientBuilder.build())
                .build()
        }

        @JvmStatic
        @NewsScope
        @Provides
        fun provideNewsApiService(
            @Named(Names.NEWS_API) retrofit: Retrofit
        ): NewsApiService {
            return retrofit.create(NewsApiService::class.java)
        }
    }

    @Binds
    abstract fun bindsNewsRepository(newsRepositoryImpl: NewsRepositoryImpl): NewsRepository

    @IntoMap
    @ViewModelKey(NewsStreamViewModel::class)
    @Binds
    abstract fun bindsNewsStreamViewModel(newsStreamViewModel: NewsStreamViewModel): ViewModel
}
