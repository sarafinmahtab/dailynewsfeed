package com.android.news.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

/**
 * @author Shamsul Arafin Mahtab
 * @since 01/03/2021
 */

@Parcelize
data class NewsSource(
    @field:SerializedName("id")
    val id: String? = null,
    @field:SerializedName("name")
    val name: String? = null
) : Parcelable
