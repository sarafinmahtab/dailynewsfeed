package com.android.news.model

import com.google.gson.annotations.SerializedName

/**
 * @author Shamsul Arafin Mahtab
 * @since 01/03/2021
 */

data class NewsResponse(
    @field:SerializedName("status")
    val status: String? = null,
    @field:SerializedName("totalResults")
    val totalResults: Int = 0,
    @field:SerializedName("articles")
    val articles: List<NewsArticle>? = null
)
