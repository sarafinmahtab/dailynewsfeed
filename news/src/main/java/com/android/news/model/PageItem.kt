package com.android.news.model

import android.os.Parcel
import android.os.Parcelable
import java.util.*

/**
 * @author Shamsul Arafin Mahtab
 * @since 07/03/2021
 */

data class PageItem<T : Parcelable>(
    val id: String = UUID.randomUUID().toString(),
    val viewType: Int,
    val totalResults: Int = 0,
    val data: T? = null
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readInt(),
        parcel.readInt(),
        when (val className = parcel.readString()) {
            null, "" -> {
                null
            }
            else -> {
                parcel.readParcelable<T>(Class.forName(className).classLoader)
            }
        }
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeInt(viewType)
        parcel.writeInt(totalResults)
        if (data != null) {
            parcel.writeString(data.javaClass.name)
        }
        parcel.writeParcelable(data, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PageItem<Parcelable>> {
        override fun createFromParcel(parcel: Parcel): PageItem<Parcelable> {
            return PageItem(parcel)
        }

        override fun newArray(size: Int): Array<PageItem<Parcelable>?> {
            return arrayOfNulls(size)
        }
    }
}
