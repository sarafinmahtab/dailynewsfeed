package com.android.news.ui.newsstream

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.core.di.ViewModelFactory
import com.android.news.databinding.FragmentNewsStreamBinding
import com.android.news.di.NewsComponentProvider
import com.android.news.ui.newsstream.adapter.NewsStreamAdapter
import javax.inject.Inject

/**
 * @author Shamsul Arafin Mahtab
 * @since 24/02/2021
 */

class NewsStreamFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private lateinit var viewModel: NewsStreamViewModel

    private lateinit var binding: FragmentNewsStreamBinding

    private lateinit var adapter: NewsStreamAdapter

    override fun onAttach(context: Context) {
        val newsComponent = (
                requireContext().applicationContext as NewsComponentProvider).provideNewsComponent()
        newsComponent.inject(this)

        viewModel = ViewModelProvider(this, viewModelFactory).get(NewsStreamViewModel::class.java)

        super.onAttach(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentNewsStreamBinding.inflate(inflater, container, false)

        adapter = NewsStreamAdapter()
        binding.newsListRecyclerView.layoutManager = LinearLayoutManager(context)
        binding.newsListRecyclerView.isNestedScrollingEnabled = false
        binding.newsListRecyclerView.adapter = adapter

        viewModel.topHeadlinesLive.observe(this, {
            adapter.submitList(it)
        })

        viewModel.loadTopHeadlines()

        return binding.root
    }

    companion object {
        const val TAG = "NewsStreamFragment"

        @JvmStatic
        fun newInstance() = NewsStreamFragment()
    }
}
