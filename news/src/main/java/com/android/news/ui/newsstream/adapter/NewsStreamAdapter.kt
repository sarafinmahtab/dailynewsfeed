package com.android.news.ui.newsstream.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.RecyclerView
import com.android.news.R
import com.android.news.data.NewsViewType
import com.android.news.databinding.ItemLoadMoreBinding
import com.android.news.databinding.ItemNewsBinding
import com.android.news.databinding.ShimmerItemNewsBinding
import com.android.news.model.NewsArticle
import com.android.news.model.PageItem
import com.squareup.picasso.Picasso

/**
 * @author Shamsul Arafin Mahtab
 * @since 07/03/2021
 */

class NewsStreamAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val mDiffer = AsyncListDiffer(this, NewsStreamDiffUtil())

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)

        return when (viewType) {
            NewsViewType.ITEM -> {
                val itemNewsBinding = ItemNewsBinding.inflate(layoutInflater, parent, false)
                NewsViewHolder(itemNewsBinding)
            }
            NewsViewType.LOAD_MORE -> {
                val itemLoadMoreBinding = ItemLoadMoreBinding.inflate(layoutInflater, parent, false)
                LoadMoreNewsViewHolder(itemLoadMoreBinding)
            }
            NewsViewType.SHIMMER_ITEM -> {
                val shimmerItemNewsBinding =
                    ShimmerItemNewsBinding.inflate(layoutInflater, parent, false)
                ShimmerNewsViewHolder(shimmerItemNewsBinding)
            }
            else -> {
                throw NotImplementedError("Illegal access of ViewType for news stream list")
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder.itemViewType == NewsViewType.ITEM) {
            val newsArticle = mDiffer.currentList[position].data
            (holder as NewsViewHolder).bind(newsArticle!!)
        }
    }

    override fun getItemCount(): Int {
        return mDiffer.currentList.size
    }

    override fun getItemViewType(position: Int): Int {
        return mDiffer.currentList[position].viewType
    }

    fun submitList(list: List<PageItem<NewsArticle>>) {
        mDiffer.submitList(list)
    }

    inner class NewsViewHolder(
        private val itemNewsBinding: ItemNewsBinding
    ) : RecyclerView.ViewHolder(itemNewsBinding.root) {

        fun bind(newsArticle: NewsArticle) {
            Picasso.get()
                .load(newsArticle.urlToImage)
                .into(itemNewsBinding.newsUrlImageView)

            itemNewsBinding.newsTitleTextView.text = newsArticle.title
            itemNewsBinding.newsSourceTextView.text =
                newsArticle.source?.name ?: itemView.context.getString(R.string.none)
        }
    }

    class LoadMoreNewsViewHolder(itemLoadMoreBinding: ItemLoadMoreBinding) :
        RecyclerView.ViewHolder(itemLoadMoreBinding.root)

    class ShimmerNewsViewHolder(
        shimmerItemNewsBinding: ShimmerItemNewsBinding
    ) : RecyclerView.ViewHolder(shimmerItemNewsBinding.root) {

        init {
            shimmerItemNewsBinding.shimmerLayout.startShimmer()
        }
    }
}
