package com.android.news.ui.newsstream.adapter

import androidx.recyclerview.widget.DiffUtil
import com.android.news.model.PageItem
import com.android.news.model.NewsArticle

/**
 * @author Shamsul Arafin Mahtab
 * @since 07/03/2021
 */

class NewsStreamDiffUtil : DiffUtil.ItemCallback<PageItem<NewsArticle>>() {

    override fun areItemsTheSame(
        oldItem: PageItem<NewsArticle>,
        newItem: PageItem<NewsArticle>
    ): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(
        oldItem: PageItem<NewsArticle>,
        newItem: PageItem<NewsArticle>
    ): Boolean {
        return oldItem == newItem
    }
}
