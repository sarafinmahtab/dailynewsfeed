package com.android.news.ui.newsstream

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.core.helpers.Results
import com.android.news.BuildConfig
import com.android.news.data.NewsViewType
import com.android.news.data.repos.NewsRepository
import com.android.news.di.NewsScope
import com.android.news.model.NewsArticle
import com.android.news.model.PageItem
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * @author Shamsul Arafin Mahtab
 * @since 01/03/2021
 */

@NewsScope
class NewsStreamViewModel @Inject constructor(
    private val newsRepository: NewsRepository
) : ViewModel() {

    private val _topHeadlinesLive = MutableLiveData<List<PageItem<NewsArticle>>>()
    val topHeadlinesLive: LiveData<List<PageItem<NewsArticle>>> = _topHeadlinesLive

    init {
        _topHeadlinesLive.value = arrayListOf(
            PageItem(viewType = NewsViewType.SHIMMER_ITEM),
            PageItem(viewType = NewsViewType.SHIMMER_ITEM),
            PageItem(viewType = NewsViewType.SHIMMER_ITEM),
            PageItem(viewType = NewsViewType.SHIMMER_ITEM),
            PageItem(viewType = NewsViewType.SHIMMER_ITEM),
            PageItem(viewType = NewsViewType.SHIMMER_ITEM),
            PageItem(viewType = NewsViewType.SHIMMER_ITEM),
            PageItem(viewType = NewsViewType.SHIMMER_ITEM)
        )
    }

    fun loadTopHeadlines(
        query: String? = null,
        country: String? = null,
        category: String? = null,
        sources: String? = null
    ) {
        viewModelScope.launch {
            val result = newsRepository.loadTopHeadlines(
                "en", BuildConfig.NEWS_API_KEY,
                country, category, sources, query
            )

            when (result) {
                is Results.Success -> {
                    val mappedList = result.value.articles?.map {
                        PageItem(
                            viewType = NewsViewType.ITEM,
                            data = it,
                            totalResults = result.value.totalResults
                        )
                    }?.toCollection(ArrayList()) ?: ArrayList()
                    if (result.value.totalResults > 20) {
                        mappedList.add(PageItem(viewType = NewsViewType.LOAD_MORE))
                    }
                    _topHeadlinesLive.postValue(mappedList)
                }
                is Results.Failure -> {
                    Log.w(TAG, "+loadTopHeadlines -> ${result.throwable}")
                }
            }
        }
    }

    companion object {
        const val TAG = "NewsStreamViewModel"
    }
}
