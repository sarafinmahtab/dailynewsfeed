package com.android.news.data.repos

import com.android.core.helpers.Results
import com.android.news.model.NewsResponse

/**
 * @author Shamsul Arafin Mahtab
 * @since 04/03/2021
 */

interface NewsRepository {

    suspend fun loadTopHeadlines(
        language: String,
        apiKey: String,
        country: String? = null,
        category: String? = null,
        sources: String? = null,
        query: String? = null
    ): Results<NewsResponse>
}
