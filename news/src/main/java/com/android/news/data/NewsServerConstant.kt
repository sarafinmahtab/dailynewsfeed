package com.android.news.data

/**
 * @author Shamsul Arafin Mahtab
 * @since 25/02/2021
 */

object NewsServerConstant {
    const val BASE_URL = "https://newsapi.org/v2/"
}
