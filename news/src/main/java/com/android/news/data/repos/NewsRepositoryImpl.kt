package com.android.news.data.repos

import com.android.core.helpers.ResolveApiResponse
import com.android.core.helpers.Results
import com.android.news.data.network.NewsApiService
import com.android.news.di.NewsScope
import com.android.news.model.NewsResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

/**
 * @author Shamsul Arafin Mahtab
 * @since 04/03/2021
 */

@NewsScope
class NewsRepositoryImpl @Inject constructor(
    private val newsApiService: NewsApiService,
    private val resolveApiResponse: ResolveApiResponse
) : NewsRepository {

    override suspend fun loadTopHeadlines(
        language: String,
        apiKey: String,
        country: String?,
        category: String?,
        sources: String?,
        query: String?
    ): Results<NewsResponse> = withContext(Dispatchers.IO) {

        return@withContext resolveApiResponse.resolve(javaClass.simpleName) {
            newsApiService.loadTopHeadlines(language, apiKey, country, category, sources, query)
        }
    }
}
