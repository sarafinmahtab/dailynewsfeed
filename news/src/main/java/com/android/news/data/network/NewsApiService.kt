package com.android.news.data.network

import com.android.news.model.NewsResponse
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * @author Shamsul Arafin Mahtab
 * @since 04/03/2021
 */

interface NewsApiService {

    @GET("everything")
    suspend fun allArticles(
        @Query("q") query: String,
        @Query("apiKey") apiKey: String
    ): NewsResponse

    @GET("top-headlines")
    suspend fun loadTopHeadlines(
        @Query("language") language: String,
        @Query("apiKey") apiKey: String,
        @Query("country") country: String? = null,
        @Query("category") category: String? = null,
        @Query("sources") sources: String? = null,
        @Query("q") query: String? = null
    ): NewsResponse
}
