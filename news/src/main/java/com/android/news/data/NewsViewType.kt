package com.android.news.data

import androidx.annotation.IntDef


/**
 * @author Shamsul Arafin Mahtab
 * @since 07/03/2021
 */

@Retention(AnnotationRetention.SOURCE)
@IntDef(
    NewsViewType.ITEM,
    NewsViewType.SHIMMER_ITEM,
    NewsViewType.LOAD_MORE
)
annotation class NewsViewType {
    companion object {
        const val ITEM = 0
        const val SHIMMER_ITEM = 1
        const val LOAD_MORE = 2
    }
}
