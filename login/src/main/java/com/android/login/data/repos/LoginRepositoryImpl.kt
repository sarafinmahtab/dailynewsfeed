package com.android.login.data.repos

import com.android.core.helpers.ResolveApiResponse
import com.android.core.helpers.Results
import com.android.core.model.LoginUser
import com.android.login.di.LoginScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject


/**
 * @author Shamsul Arafin Mahtab
 * @since 15/03/2021
 */

@LoginScope
class LoginRepositoryImpl @Inject constructor(
    private val resolveApiResponse: ResolveApiResponse
) : LoginRepository {

    override suspend fun startLogin(username: String, password: String): Results<LoginUser> =
        withContext(Dispatchers.Main) {
            return@withContext resolveApiResponse.resolve(javaClass.simpleName) {
                LoginUser(username, password)
            }
        }
}
