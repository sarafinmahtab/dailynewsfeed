package com.android.login.data.repos

import com.android.core.helpers.Results
import com.android.core.model.LoginUser


/**
 * @author Shamsul Arafin Mahtab
 * @since 15/03/2021
 */

interface LoginRepository {

    suspend fun startLogin(username: String, password: String): Results<LoginUser>
}
