package com.android.login.di

/**
 * @author Shamsul Arafin Mahtab
 * @since 15/03/2021
 */

interface LoginComponentProvider {
    fun provideLoginComponent(): LoginComponent
}
