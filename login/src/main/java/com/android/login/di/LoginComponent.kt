package com.android.login.di

import com.android.login.ui.login.LoginActivity
import dagger.Subcomponent


/**
 * @author Shamsul Arafin Mahtab
 * @since 15/03/2021
 */

@LoginScope
@Subcomponent(
    modules = [LoginDiModule::class]
)
interface LoginComponent {

    fun inject(loginActivity: LoginActivity)

    @Subcomponent.Factory
    interface Factory {

        fun create(): LoginComponent
    }
}
