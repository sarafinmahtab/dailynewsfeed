package com.android.login.di

import androidx.lifecycle.ViewModel
import com.android.core.di.ViewModelKey
import com.android.login.data.repos.LoginRepository
import com.android.login.data.repos.LoginRepositoryImpl
import com.android.login.ui.login.LoginViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap


/**
 * @author Shamsul Arafin Mahtab
 * @since 15/03/2021
 */

@Module
abstract class LoginDiModule {

    @Binds
    abstract fun bindsLoginRepository(loginRepositoryImpl: LoginRepositoryImpl): LoginRepository

    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    @Binds
    abstract fun bindsLoginViewModel(loginViewModel: LoginViewModel): ViewModel
}
