package com.android.login.di

import javax.inject.Scope

/**
 * @author Shamsul Arafin Mahtab
 * @since 15/03/2021
 */

@Scope
@Retention(value = AnnotationRetention.RUNTIME)
annotation class LoginScope
