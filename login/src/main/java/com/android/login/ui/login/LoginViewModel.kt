package com.android.login.ui.login

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.core.usecase.UserContext
import com.android.core.helpers.Results
import com.android.core.model.LoginUser
import com.android.login.data.repos.LoginRepository
import com.android.login.di.LoginScope
import kotlinx.coroutines.launch
import javax.inject.Inject


/**
 * @author Shamsul Arafin Mahtab
 * @since 15/03/2021
 */

@LoginScope
class LoginViewModel @Inject constructor(
    private val loginRepository: LoginRepository,
    private val userContext: UserContext
) : ViewModel() {

    private val _loginUserLive = MutableLiveData<LoginUser>()
    val loginUserLive: LiveData<LoginUser> = _loginUserLive

    fun login(username: String, password: String) {
        viewModelScope.launch {
            when (val result = loginRepository.startLogin(username, password)) {
                is Results.Success -> {
                    saveLoginToUserContext(result.value)
                    _loginUserLive.postValue(result.value)
                }
                is Results.Failure -> {
                    Log.w(TAG, result.throwable)
                }
            }
        }
    }

    private fun saveLoginToUserContext(loginUser: LoginUser) {
        viewModelScope.launch {
            userContext.saveLoginUser(loginUser)
        }
    }

    companion object {
        private const val TAG = "LoginViewModel"
    }
}
