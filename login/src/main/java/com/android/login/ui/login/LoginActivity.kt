package com.android.login.ui.login

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.android.core.usecase.IntentLauncher
import com.android.core.di.ViewModelFactory
import com.android.login.R
import com.android.login.databinding.ActivityLoginBinding
import com.android.login.di.LoginComponentProvider
import javax.inject.Inject

/**
 * @author Shamsul Arafin Mahtab
 * @since 15/03/2021
 */

class LoginActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private lateinit var viewModel: LoginViewModel

    @Inject
    lateinit var newFeedIntentLauncher: IntentLauncher

    override fun onCreate(savedInstanceState: Bundle?) {
        val loginComponent = (applicationContext as LoginComponentProvider).provideLoginComponent()
        loginComponent.inject(this)

        viewModel = ViewModelProvider(this, viewModelFactory).get(LoginViewModel::class.java)

        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.loginButton.setOnClickListener {
            prepareLogin()
        }

        viewModel.loginUserLive.observe(this) {
            val intent = newFeedIntentLauncher.getNewsFeedIntent(this)
            startActivity(intent)
            finish()
        }
    }

    private fun prepareLogin() {
        val username = binding.usernameEntry.text.toString()
        val password = binding.passwordEntry.text.toString()

        if (username.isBlank()) {
            binding.usernameEntry.error = getString(R.string.username_is_empty)
            return
        }

        if (password.isBlank()) {
            binding.passwordEntry.error = getString(R.string.password_is_empty)
            return
        }

        if (password.length < 4) {
            binding.passwordEntry.error = getString(R.string.password_is_not_valid)
            return
        }

        viewModel.login(username, password)
    }
}
