package com.android.tvshows.di

import javax.inject.Scope

/**
 * @author Shamsul Arafin Mahtab
 * @since 04/03/2021
 */

@Scope
@Retention(value = AnnotationRetention.RUNTIME)
annotation class TvShowsScope
