package com.android.tvshows.di

import com.android.newsapp.di.AppComponent
import com.android.tvshows.ui.tvshows.OnAirShowsFragment
import dagger.Component

/**
 * @author Shamsul Arafin Mahtab
 * @since 04/03/2021
 */

@TvShowsScope
@Component(
    dependencies = [AppComponent::class]
)
interface TvShowsComponent {

    @Component.Factory
    interface Factory {
        // Takes an instance of AppComponent when creating
        // an instance of LoginComponent
        fun create(appComponent: AppComponent): TvShowsComponent
    }

    fun inject(onAirShowsFragment: OnAirShowsFragment)
}
