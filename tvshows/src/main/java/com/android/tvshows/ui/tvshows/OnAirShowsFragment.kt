package com.android.tvshows.ui.tvshows

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.android.tvshows.databinding.FragmentOnAirShowsBinding

/**
 * @author Shamsul Arafin Mahtab
 * @since 04/03/2021
 */

class OnAirShowsFragment : Fragment() {

    private lateinit var binding: FragmentOnAirShowsBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
//        val appComponent = (requireContext().applicationContext as DailyNewsFeed).appComponent
//
//        DaggerTvShowsComponent.factory().create(appComponent).inject(this)

        binding = FragmentOnAirShowsBinding.inflate(inflater, container, false)

        return binding.root
    }

    companion object {

        @JvmStatic
        fun newInstance() = OnAirShowsFragment()
    }
}
